from django.urls import path
from acinsite import views
app_name='Main'

urlpatterns=[
    path('',views.main, name="main"),
    path('login/',views.login,name="login"),
    path('logout/',views.logout,name="logout"),
    path('intro/',views.intro,name="intro"),
    path('professor/',views.professor,name="professor"),
    path('member/',views.member,name="member"),
    path('project/create/',views.create_project,name="create-project"),
    path('project/delete/<pk>/',views.delete_project,name="delete-project"),
    path('project/modify/list/',views.modify_list,name="modify-list"),
    path('project/ajax/',views.get_ajax_project,name="ajax-project"),
    path('project/',views.project,name="project"),
    path('project/<keyword>/',views.project,name="project"),
    path('gallery/write/',views.write_gallery,name="write-gallery"),
    path('gallery/',views.gallery,name="gallery"),
    path('gallery/detail/<int:pk>/',views.detail_gallery,name="detail-gallery"),
    path('gallery/delete/<int:pk>/',views.delete_gallery,name="delete-gallery"),
    #MYPAGE
    path('mypage/',views.mypage,name="mypage"),
    
    #게시판
    path('board/Viewboard/',views.Viewboard,name="Viewboard"),
    path('board/Writeboard/',views.Writeboard,name="Writeboard"),
    path('board/Viewboard/Boarddetail/<int:pk>',views.Boarddetail,name="Boarddetail"),
    path('board/Viewboard/Boarddetail/Deleteboard/<int:pk>',views.Deleteboard, name="Deleteboard"),
    path('board/Viewboard/Boarddetail/Editboard/<int:pk>',views.Editboard, name="Editboard"),

    #일일보고
    path('Today_Report_Today_Report_Board',views.Today_Report_Board, name="Today_Report_Board"),
    path('Write_Today_Report', views.Write_Today_Report,name="Write_Today_Report"),
    path('Today_Report_detail/<int:pk>/',views.Today_Report_detail,name="Today_Report_detail"),
    path('Today_Report_Edit/<int:pk>/',views.Today_Report_Edit,name="Today_Report_Edit"),
    path('Today_Report_Del/<int:pk>',views.Today_Report_Del,name="Today_Report_Del"),
    

]
