from django.apps import AppConfig


class AcinsiteConfig(AppConfig):
    name = 'acinsite'
