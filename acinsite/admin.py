from django.contrib import admin
from acinsite.models import *

admin.site.register(User)
admin.site.register(Performance)
admin.site.register(User_perform)
admin.site.register(Board)
admin.site.register(Comment)
admin.site.register(Reply)
admin.site.register(Gallery)
admin.site.register(Gallery_reference)
admin.site.register(Today_Report)
