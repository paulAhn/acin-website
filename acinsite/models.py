from django.db import models
#유저
class User(models.Model):
    user_number = models.CharField(max_length=20,null=True,blank=True, verbose_name="학번")
    user_pw = models.CharField(max_length=20 ,null=True,blank=True, verbose_name="비밀번호")
    user_name = models.CharField(max_length=20, null=True,blank=True, verbose_name="이름")
    user_email = models.EmailField(null=True,blank=True,verbose_name="이메일")
    user_major = models.CharField(max_length=20, null=True,blank=True, verbose_name="학과")
    user_profile = models.ImageField(blank=True, verbose_name="프로필 사진")
    user_birthday = models.DateField(null=True,blank=True, verbose_name="생년월일")
    user_phone = models.CharField(max_length=11,null=True,blank=True,verbose_name="핸드폰번호")
    user_ranklist = (('1','교수'),('2','팀장'),('3','팀원'))
    user_rank = models.CharField(max_length=10, choices=user_ranklist, null=True,blank=True,verbose_name="직책")
    user_interest  = models.CharField(max_length=40,null=True,blank=True,verbose_name="관심분야1")
    user_interest2 = models.CharField(max_length=40,null=True,blank=True,verbose_name="관심분야2")
    user_interest3 = models.CharField(max_length=40,null=True,blank=True,verbose_name="관심분야3")
    user_highschool = models.CharField(max_length=20, null=True,blank=True,verbose_name="출신 고등학교")
    user_highschool_date = models.DateField(null=True,blank=True, verbose_name="고등학교 졸업일")
    user_university = models.CharField(max_length=20,null=True,blank=True, verbose_name="출신 대학교")
    user_university_date = models.DateField(null=True,blank=True,verbose_name='대학 졸업일')
    user_master = models.CharField(max_length=20,null=True,blank=True,verbose_name="석사")
    user_master_date = models.DateField(null=True,blank=True,verbose_name="석사임명 날짜")
    user_doctor = models.CharField(max_length=20,null=True,blank=True,verbose_name="박사")
    user_doctor_date = models.DateField(null=True,blank=True,verbose_name="박사임명 날짜")

    def __str__(self):
        return self.user_name

# 프로젝트
class Performance(models.Model):
    perf_title = models.CharField(max_length=200,null=True,blank=True, verbose_name="프로젝트 제목")
    perf_typelist =(('1','프로젝트'),('2','논문'),('3','지적재산권'))
    perf_type = models.CharField(max_length=10,choices=perf_typelist,null=True,blank=True,verbose_name="프로젝트 종류")
    perf_content = models.TextField(max_length=200,null=True,blank=True, verbose_name="내용")
    perf_etc = models.TextField(max_length=100,null=True,blank=True, verbose_name="비고")
    perf_host = models.CharField(max_length=20,null=True,blank=True , verbose_name="주관")
    perf_sponsor = models.CharField(max_length=20,null=True,blank=True, verbose_name="발주")
    perf_state = models.BooleanField(default=False, verbose_name="상태")
    perf_start = models.DateField(null=True,blank=True, verbose_name="프로젝트 시작일")
    perf_end = models.DateField(null=True,blank=True, verbose_name="프로젝트 종료일")
    perf_end_year = models.CharField(max_length=4,null=True,blank=True,verbose_name="종료연도")
    perf_file = models.FileField(null=True,blank=True, verbose_name="파일",upload_to="project/")
    perf_isProfessor = models.BooleanField(default=False, verbose_name="교수님 프로젝트")


    def __str__(self):
        return self.perf_title
# 유저와 프로젝트 연결
class User_perform(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE ,null=True,blank=False)
    other_user = models.CharField(max_length=20 , null=True, blank=True)
    performance = models.ForeignKey(Performance,on_delete=models.CASCADE,related_name="perf",null=True,blank=False)


#게시판
class Board(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=False,verbose_name="작성자")
    board_title = models.CharField(max_length=100,null=True,blank=False,verbose_name="게시판 제목")
    board_content = models.TextField(max_length=500,null=True,blank=False,verbose_name="내용")
    board_date = models.DateTimeField(null=True,blank=False, verbose_name="작성일")
    board_hits = models.IntegerField(default=0,verbose_name="조회수")
    board_file = models.FileField(null=True,blank=True, verbose_name="파일")

    def __str__(self):
        return self.board_title
#댓글
class Comment(models.Model):
    board = models.ForeignKey(Board,on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True,blank=False)
    comment_content = models.TextField(max_length=30,null=True,blank=True,verbose_name="댓글")
    comment_date = models.DateTimeField(null=True,blank=False, verbose_name="작성시간")

    def __str__(self):
        return self.comment_content
#대댓글
class Reply(models.Model):
    comment = models.ForeignKey(Comment,on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE,null=True,blank=False)
    reply_content = models.TextField(null=True,blank=True,verbose_name="대댓글")
    reply_date = models.DateTimeField(null=True,blank=False,verbose_name="대댓글 작성시간")

    def __str__(self):
        return self.reply_content

def gallery_img_upload(instance, file):

    return "gallery/".format(instance.gal_title,file)

def gal_ref_img_upload(instance,file):
    return "gallery/{}/{}".format(instance.gallery.id,file)


#겔러리
class Gallery(models.Model):
    user = models.ForeignKey(User,null=True,blank=False, on_delete=models.CASCADE)
    gal_title = models.CharField(max_length=100,null=True,blank=False,verbose_name="제목")
    gal_date = models.DateField(null=True,blank=False,verbose_name="작성일",auto_now_add=True)
    gal_hits = models.IntegerField(default=0, verbose_name="조회수")
    gal_file = models.FileField(null=True,blank=True,upload_to=gallery_img_upload,verbose_name="파일")
    def __str__(self):
        return self.gal_title



#겔러리 여러사진 등록
class Gallery_reference(models.Model):
    gallery = models.ForeignKey(Gallery,related_name="galleryref",on_delete=models.CASCADE,)
    gal_photo = models.ImageField(null=True,blank=True,verbose_name="사진",upload_to=gal_ref_img_upload)


#일일보고
class Today_Report(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=False,verbose_name="작성자")
    Today_Report_title = models.CharField(max_length=100,null=True,blank=False,verbose_name="게시판 제목")
    Today_Report_content = models.TextField(max_length=500,null=True,blank=False,verbose_name="내용")
    Today_Report_date = models.DateTimeField(null=True,blank=False, verbose_name="작성일")
    Today_Report_hits = models.IntegerField(default=0,verbose_name="조회수")
    Today_Report_file = models.FileField(null=True,blank=True, verbose_name="파일")

    def __str__(self):
        return self.Today_Report_title
