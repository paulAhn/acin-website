from django.shortcuts import render,redirect,get_object_or_404,HttpResponse
from acinsite.models import *
from acinsite.forms import  *
from django.utils import timezone
from datetime import datetime
#메인
def main(request):
    cur_date = datetime.now().date()
    gal_objs = Gallery.objects.all().order_by('-id')[0:4]
    board_objs = Board.objects.all().order_by('-id')[0:3]
    user_birthday = User.objects.all()
    today_birth_user = User.objects.filter(user_birthday__month=cur_date.month,
                                           user_birthday__day=cur_date.day)
    if today_birth_user:
        today_birth_user = today_birth_user[0]
    context = {
        'cur_date':cur_date,
        'gal_objs':gal_objs,
        'board_objs':board_objs,
        'user_birthday':user_birthday,
        'today_birth_user':today_birth_user
    }
    return render(request,'acinsite/main.html',context)
#로그인
def login(request):
    context = {
        'msg' : '',
    }
    if request.method == "POST":
        user_objs = User.objects.filter(
                        user_number = request.POST.get('user_id'),
                        user_pw = request.POST.get('user_pw')
                    )
        if user_objs:
            request.session['user_id']  = request.POST.get('user_id')
            request.session['user_pk']  = user_objs[0].pk
            request.session['user_name']= user_objs[0].user_name
            return redirect("/")
        else:
            context['msg'] = 'LoginFail'
    return render(request,'acinsite/login.html',context)

#로그아웃
def logout(request):
    if request.session.get('user_id') or request.session.get('user_name'):
        del request.session['user_id']
        del request.session['user_name']
        del request.session['user_pk']


    return redirect('/')
#소개 페이지
def intro(request):
    return render(request,'acinsite/intro.html')
#professor 페이지
def professor(request):
    user = get_object_or_404(User,user_rank=1)
    objects = User_perform.objects.filter(
                    user=user,
                    performance__perf_state=True
                ).order_by('-performance__perf_end')

    print(user)
    print(objects)
    context = {
        'user':user,
        'objects':objects,
    }
    return render(request,'acinsite/professor.html',context)
#멤버 패이지
def member(request):
    users = User.objects.exclude(user_rank=1).order_by('user_rank','user_birthday')
    print(users)
    context = {
        'users':users,
    }
    return render(request,'acinsite/member.html',context)

#프로젝트 메인
def project(request,keyword=None):
    perf_objs = None
    context = {}
    #진행중인 프로젝트
    if keyword == "proceeding":
        perf_objs = Performance.objects.filter(
                    perf_isProfessor=False,
                    perf_state=False
                ).order_by('-perf_start')
        context['type']='proceeding'
    #종료된 프로젝트
    else:
        year_list = []
        perf_objs = Performance.objects.filter(
                    perf_isProfessor=False,
                    perf_state=True
                ).order_by('-perf_end')

        for obj in perf_objs:
            year = obj.perf_end_year
            if not year in year_list:
                year_list.append(year)
        context['year_list']=year_list
        context['type']='finish'

    context['object_list']=perf_objs

    return render(request,'acinsite/project/project_list.html',context)
#ajax 요청 프로젝트
def get_ajax_project(request):
    object_list = None
    if request.POST.get('year') == "all":
        object_list = Performance.objects.filter(
                            perf_isProfessor=False,
                            perf_state=True).order_by('-perf_end')
    else:
        object_list = Performance.objects.filter(
                            perf_isProfessor=False,
                            perf_state=True,
                            perf_end_year=request.POST.get('year')).order_by('-perf_end')
    context = {
        'object_list':object_list
    }
    return render(request,'acinsite/project/ajax_project.html',context)

#프로젝트 생성
def create_project(request):
    if not request.session.get('user_id'):
        return redirect('/login')
    pk = request.GET.get('pk')
    print(pk)
    if request.method == "POST":
        perf_status = False
        start_date = None
        end_date = None
        end_year = None
        if pk:
            print("hello")
            obj = Performance.objects.get(pk=pk)
            obj.delete()

        if request.POST.get('project_status'):
            perf_status=True
        if request.POST.get("start_date") != "":
            start_date = request.POST.get("start_date")
        if request.POST.get("end_date") != "":
            end_date = request.POST.get("end_date")
            end_year = request.POST.get("end_date")[0:4]


        perf_obj = Performance.objects.create(
                        perf_title = request.POST.get('project_title'),
                        perf_type  = request.POST.get('project_type'),
                        perf_content=request.POST.get('project_content'),
                        perf_etc=request.POST.get('project_etc'),
                        perf_host=request.POST.get('project_host'),
                        perf_sponsor=request.POST.get('project_sponso'),
                        perf_state=perf_status,
                        perf_start=start_date,
                        perf_end=end_date,
                        perf_end_year=end_year,
                    )
        if request.FILES.get('file'):
            perf_obj.perf_file=request.FILES.get('file')
        else:
            perf_obj.perf_file=request.POST.get('old_file')
        perf_obj.save(update_fields=['perf_file'])

        for mem in request.POST.getlist('member'):
            if isNumber(mem):
                user = get_object_or_404(User, id=mem)
                User_perform.objects.create(user=user,performance=perf_obj)
            else:
                User_perform.objects.create(other_user=mem,performance=perf_obj)

        return redirect("/project")

    #GET 방식
    context={}

    obj=None

    if pk:
        obj = get_object_or_404(Performance,pk=pk)
        context['obj']=obj


    users = User.objects.all().order_by('user_rank','user_birthday')
    context['users']=users
    return render(request,'acinsite/project/create_project.html',context)

def modify_list(request):
    # GET
    if not request.session.get('user_id'):
        return redirect('/login')
    if request.method == "GET":
        object_list = Performance.objects.exclude(perf_isProfessor=True).order_by('perf_state')
        print(object_list)
        context = {
            'object_list':object_list,
        }
        return render(request,'acinsite/project/modify_list.html',context)


def delete_project(request,pk):
    obj = get_object_or_404(Performance,pk=pk)
    obj.delete()
    return redirect("/project/modify/list/")

def one_page_count(objs,page):
    total_row = 12
    start = total_row * (page-1)
    end   = total_row + start


    return objs.count(),objs[start:end]

def gallery(request):
    context = {}
    object_list = None
    total_count = None
    page = int(request.GET.get('page'))
    if request.GET.get('keyword') == "modify":
        user_id = request.session.get('user_id')
        user=get_object_or_404(User,user_number=user_id)
        objs=Gallery.objects.filter(user=user)
        total_count,object_list = one_page_count(objs,page)
        context['keyword'] = "modify"

    else:
        objs = Gallery.objects.all().order_by('-id')
        total_count,object_list = one_page_count(objs,page)


    context['object_list'] = object_list
    context['page'] = page
    context['total_count'] = total_count
    return render(request,'acinsite/gallery/gallery.html',context)

#갤러리 쓰기
def write_gallery(request):
    if not request.session.get('user_id'):
        return redirect('/login')
    context = {}
    pk = request.GET.get('pk')
    if request.method == "POST":
        atch_file = None
        files = []
        #기존 테이블 삭제
        if pk:
            Gallery.objects.get(pk=pk).delete()
        user = get_object_or_404(User,user_number=request.session.get('user_id'))
        if request.FILES.get('atchdFile'):
            atch_file = request.FILES.get('atchdFile')
        elif request.POST.get('atchdFile'):
            atch_file = request.POST.get('atchdFile')

        gal_obj = Gallery.objects.create(
                        user=user,
                        gal_title=request.POST.get('title'),
                        gal_file =atch_file,
                    )

        if request.FILES.getlist('file'):
            files=request.FILES.getlist('file')
        if request.POST.getlist('file-mfy'):
            files+=request.POST.getlist('file-mfy')


        for file in files:
            Gallery_reference.objects.create(
                gallery=gal_obj,
                gal_photo=file
            )
        return HttpResponse("성공")
    if pk:
        obj = get_object_or_404(Gallery,pk=pk)
        context['obj'] = obj

    return render(request,'acinsite/gallery/write_gallery.html',context)
def detail_gallery(request,pk):

    obj = get_object_or_404(Gallery,pk=pk)
    obj.gal_hits = obj.gal_hits + 1
    obj.save(update_fields=['gal_hits'])
    context = {
        'obj':obj,
    }
    return render(request,'acinsite/gallery/detail_gallery.html',context)
def delete_gallery(request,pk):
    obj = get_object_or_404(Gallery,pk=pk)
    obj.delete()
    return redirect('/gallery?page=1')
#숫자 이냐?
def isNumber(number):
    try:
        int(number)
        return True
    except ValueError:
        return False

#mypage
def mypage(request):
    user_id = request.session.get('user_id')
    user = User.objects.get(user_number=user_id)
    print(user)
    context = {
        'user':user,
    }
    return render(request,'acinsite/mypage/member_info.html',context)
def Viewboard(request):
    vb = Board.objects.all().order_by('-pk')
    return render(request,'acinsite/board/Viewboard.html',{'vb':vb})

def Writeboard(request):
    user_id = request.session.get('user_id')
    if not user_id :
        return redirect('/login')
    if request.method == 'POST':


        form = PostForm(request.POST, request.FILES)

        # print(form.errors.as_data())

        if form.is_valid():
            Board = form.save(commit=False)
            Board.user = User.objects.get(user_number=user_id)
            Board.board_title = request.POST['board_title']
            Board.board_content = request.POST['board_content']
            Board.board_date = timezone.now()
            Board.save()
            return redirect('Main:Viewboard')
    else:
        form = PostForm()
    return render(request,'acinsite/board/Writeboard.html',{'form':form})

# 게시판 제목클릭시 들어가는화면
def Boarddetail(request, pk):
    de = Board.objects.get(pk=pk)
    de.board_hits = de.board_hits + 1
    de.save(update_fields=['board_hits'])
    next_board = Board.objects.filter(pk__gt=pk)
    if next_board:
        next_board = next_board[0]

    prev_board = Board.objects.filter(pk__lt=pk)
    if prev_board:
        prev_board = prev_board[0]

    context = {
        'de':de,
        'next_board':next_board,
        'prev_board':prev_board
    }
    return render(request, 'acinsite/board/Boarddetail.html',context)

#게시판 삭제
def Deleteboard(request,pk):
    db = Board.objects.get(pk=pk)
    db.delete()
    return redirect('Main:Viewboard')


#게시판 수정
def Editboard(request, pk):
    eb = Board.objects.get(pk=pk)
    if request.method == "POST":
        eb.board_title = request.POST['board_title']
        eb.board_content = request.POST['board_content']
        if request.POST.get('clear') or request.FILES.get('file'):
            eb.board_file = request.FILES.get('file')


        eb.save()
        return redirect('Main:Viewboard')
    return render(request,'acinsite/board/Editboard.html',{'eb':eb})

#일일보고
def Today_Report_Board(request):
    vb = Today_Report.objects.all().order_by('-pk')
    return render(request,'acinsite/today_Report/Today_Report_Board.html',{'vb':vb})

def Write_Today_Report(request):
    user_id = request.session.get('user_id')
    if not user_id :
        return redirect('/login')
    if request.method == 'POST':
        form = Today_Report_Form(request.POST, request.FILES)

        print(form.errors.as_data())

        if form.is_valid():
            Today_Report = form.save(commit=False)
            Today_Report.user = User.objects.get(user_number=user_id)
            Today_Report.Today_Report_title = request.POST['Today_Report_title']
            Today_Report.Today_Report_content = request.POST['Today_Report_content']
            Today_Report.Today_Report_date = timezone.now()
            Today_Report.save()
            return redirect('Main:Today_Report_Board')
    else:
        form = Today_Report_Form()
    return render(request,'acinsite/today_Report/Write_Today_Report.html',{'form':form})

def Today_Report_detail(request,pk):

    de = Today_Report.objects.get(pk=pk)
    de.Today_Report_hits = de.Today_Report_hits + 1
    de.save(update_fields=['Today_Report_hits'])
    next_board = Today_Report.objects.filter(pk__gt=pk)
    if next_board:
        next_board = next_board[0]

    prev_board = Today_Report.objects.filter(pk__lt=pk)
    if prev_board:
        prev_board = prev_board[0]

    context = {
        'de':de,
        'next_board':next_board,
        'prev_board':prev_board
    }
    return render(request, 'acinsite/today_Report/Today_Report_detail.html',context)

def Today_Report_Edit(request, pk):
    eb = Today_Report.objects.get(pk=pk)
    if request.method == "POST":
        eb.Today_Report_title = request.POST['Today_Report_title']
        eb.Today_Report_content = request.POST['Today_Report_content']
        if request.POST.get('clear') or request.FILES.get('file'):
            eb.Today_Report_file = request.FILES.get('file')


        eb.save()
        return redirect('Main:Today_Report_Board')
    return render(request,'acinsite/today_Report/Today_Report_Edit.html',{'eb':eb})


def Today_Report_Del(request,pk):
    db = Today_Report.objects.get(pk=pk)
    db.delete()
    return redirect('Main:Today_Report_Board')
