from django import forms
from django_summernote.widgets import SummernoteWidget
from django_summernote import fields as summer_fields
from .models import User,Performance,User_perform,Board,Comment,Reply,Gallery,Gallery_reference, Today_Report

class PostForm(forms.ModelForm):
    board_content = forms.CharField(widget=SummernoteWidget())
    class Meta:
        model = Board
        fields = ('board_title','board_content','board_file')
        widget = { 
            
            'board_content' : forms.Textarea(attrs={'cols':100, 'rows':100}),
        }
    def __init__(self, *args, **kwargs):
        super(PostForm, self).__init__(*args, **kwargs)
        self.fields['board_file'].required = False
        self.fields['board_title'].widget.attrs.update({'class':'form-control'})

class Today_Report_Form(forms.ModelForm):
    Today_Report_content = forms.CharField(widget=SummernoteWidget())
    class Meta:
        model = Today_Report
        fields = ('Today_Report_title','Today_Report_content','Today_Report_file')
        widget = {

            'Today_Report_content' : forms.Textarea(attrs={'cols':100, 'rows':100}),
        }
    def __init__(self, *args, **kwargs):
        super(Today_Report_Form, self).__init__(*args, **kwargs)
        self.fields['Today_Report_file'].required = False
        self.fields['Today_Report_title'].widget.attrs.update({'class':'form-control'})
