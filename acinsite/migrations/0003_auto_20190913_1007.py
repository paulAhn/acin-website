# Generated by Django 2.2 on 2019-09-13 10:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('acinsite', '0002_auto_20190911_1458'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='user_email',
            field=models.EmailField(blank=True, max_length=254, null=True, verbose_name='이메일'),
        ),
        migrations.AddField(
            model_name='user',
            name='user_profile',
            field=models.ImageField(blank=True, upload_to='', verbose_name='프로필 사진'),
        ),
        migrations.AddField(
            model_name='user_perform',
            name='other_user',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='performance',
            name='perf_content',
            field=models.TextField(blank=True, max_length=200, null=True, verbose_name='내용'),
        ),
        migrations.AlterField(
            model_name='performance',
            name='perf_end',
            field=models.DateField(blank=True, null=True, verbose_name='프로젝트 종료일'),
        ),
        migrations.AlterField(
            model_name='performance',
            name='perf_etc',
            field=models.TextField(blank=True, max_length=100, null=True, verbose_name='비고'),
        ),
        migrations.AlterField(
            model_name='performance',
            name='perf_file',
            field=models.FileField(blank=True, null=True, upload_to='', verbose_name='파일'),
        ),
        migrations.AlterField(
            model_name='performance',
            name='perf_host',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='주관'),
        ),
        migrations.AlterField(
            model_name='performance',
            name='perf_sponsor',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='발주'),
        ),
        migrations.AlterField(
            model_name='performance',
            name='perf_start',
            field=models.DateField(blank=True, null=True, verbose_name='프로젝트 시작일'),
        ),
        migrations.AlterField(
            model_name='performance',
            name='perf_title',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='프로젝트 제목'),
        ),
        migrations.AlterField(
            model_name='performance',
            name='perf_type',
            field=models.CharField(blank=True, choices=[('1', '프로젝트'), ('2', '논문'), ('3', '지적재산권')], max_length=10, null=True, verbose_name='프로젝트 종류'),
        ),
        migrations.AlterField(
            model_name='user',
            name='user_birthday',
            field=models.DateField(blank=True, null=True, verbose_name='생년월일'),
        ),
        migrations.AlterField(
            model_name='user',
            name='user_doctor',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='박사'),
        ),
        migrations.AlterField(
            model_name='user',
            name='user_doctor_date',
            field=models.DateField(blank=True, null=True, verbose_name='박사임명 날짜'),
        ),
        migrations.AlterField(
            model_name='user',
            name='user_highschool',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='출신 고등학교'),
        ),
        migrations.AlterField(
            model_name='user',
            name='user_highschool_date',
            field=models.DateField(blank=True, null=True, verbose_name='고등학교 졸업일'),
        ),
        migrations.AlterField(
            model_name='user',
            name='user_major',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='학과'),
        ),
        migrations.AlterField(
            model_name='user',
            name='user_master',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='석사'),
        ),
        migrations.AlterField(
            model_name='user',
            name='user_master_date',
            field=models.DateField(blank=True, null=True, verbose_name='석사임명 날짜'),
        ),
        migrations.AlterField(
            model_name='user',
            name='user_name',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='이름'),
        ),
        migrations.AlterField(
            model_name='user',
            name='user_number',
            field=models.IntegerField(blank=True, null=True, verbose_name='학번'),
        ),
        migrations.AlterField(
            model_name='user',
            name='user_phone',
            field=models.IntegerField(blank=True, null=True, verbose_name='핸드폰번호'),
        ),
        migrations.AlterField(
            model_name='user',
            name='user_pw',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='비밀번호'),
        ),
        migrations.AlterField(
            model_name='user',
            name='user_rank',
            field=models.CharField(blank=True, choices=[('1', '교수'), ('2', '팀장'), ('3', '팀원')], max_length=10, null=True, verbose_name='직책'),
        ),
        migrations.AlterField(
            model_name='user',
            name='user_university',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='출신 대학교'),
        ),
        migrations.AlterField(
            model_name='user',
            name='user_university_date',
            field=models.DateField(blank=True, null=True, verbose_name='대학 졸업일'),
        ),
    ]
