# Generated by Django 2.1.12 on 2020-02-26 19:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('acinsite', '0016_auto_20190924_1643'),
    ]

    operations = [
        migrations.CreateModel(
            name='Today_Report',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Today_Report_title', models.CharField(max_length=100, null=True, verbose_name='게시판 제목')),
                ('Today_Report_content', models.TextField(max_length=500, null=True, verbose_name='내용')),
                ('Today_Report_date', models.DateTimeField(null=True, verbose_name='작성일')),
                ('Today_Report_hits', models.IntegerField(default=0, verbose_name='조회수')),
                ('Today_Report_file', models.FileField(blank=True, null=True, upload_to='', verbose_name='파일')),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='acinsite.User', verbose_name='작성자')),
            ],
        ),
        migrations.RemoveField(
            model_name='performance',
            name='perf_end',
        ),
        migrations.RemoveField(
            model_name='performance',
            name='perf_end_year',
        ),
        migrations.RemoveField(
            model_name='performance',
            name='perf_etc',
        ),
        migrations.RemoveField(
            model_name='performance',
            name='perf_file',
        ),
        migrations.RemoveField(
            model_name='performance',
            name='perf_host',
        ),
        migrations.RemoveField(
            model_name='performance',
            name='perf_isProfessor',
        ),
        migrations.RemoveField(
            model_name='performance',
            name='perf_sponsor',
        ),
        migrations.RemoveField(
            model_name='performance',
            name='perf_start',
        ),
        migrations.RemoveField(
            model_name='performance',
            name='perf_state',
        ),
    ]
