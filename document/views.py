from django.shortcuts import render, HttpResponse, get_object_or_404, redirect
from django.http import JsonResponse
from django.views.generic.edit import CreateView
from django.views.generic import View
from django.core import serializers
from .models import *
from acinsite.models import User
from datetime import date
from django.db.models import Q
from django.core.paginator import Paginator
from datetime import datetime
# Create your views here.
def main(request):
    user = get_object_or_404(User, pk=request.session['user_pk'])
    
    q = Q()
    q.add(Q(dr_drafter=user), q.OR)
    q.add(Q(dr_addressee=user), q.OR)

    page = request.GET.get('page', 1)
    reports = DailyReport.objects.filter(q).order_by('-dr_created_at')
    reports, paginator_obj, _range = getPaginatorDatas(page, reports, 20)
    return render(request, 'document/document_main.html', {
        'reports' : reports,
        'p' : paginator_obj,
        'range' : _range
    })

class ReportRead(View):
    def get(self, reqeust, *args, **kwargs):
        report = get_object_or_404(DailyReport, pk=kwargs.get('pk'))
        # 읽을 수 있는가 확인
        user = get_object_or_404(User, pk=reqeust.session['user_pk'])
        if report.dr_drafter == user or report.dr_addressee == user:
            
            return render(reqeust, 'document/dailyReport/read.html', {
                'report' : report,
            })

    # 결재 처리
    def post(self, request, *args, **kwargs):
        print(request.POST)
        report = get_object_or_404(DailyReport, pk=kwargs.get('pk'))
        
        # 이미 처리가 되었다면 처리 불가
        if report.dr_status != '1':
            return JsonResponse({
                'msg' : '이미 결재 완료된 보고서입니다.',
            }, status=200)
        
        # 데이터 변경
        report.dr_status = request.POST.get('status')
        report.dr_feedback = request.POST.get('feedback')
        report.dr_checked_at = datetime.today().strftime("%Y-%m-%d %H:%M:%S") 
        report.save(update_fields=['dr_status', 'dr_feedback', 'dr_checked_at'])
        return JsonResponse({
            'msg' : '{0}의 보고서가 {1} 처리 되었습니다.'.format(report.dr_title, report.get_dr_status_display()),
        }, status=200)


class ReportWrite(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'document/dailyReport/write.html', {})

    def post(self, request, *args, **kwargs):
        print(request.session['user_pk'])
        
        dr_drafter = get_object_or_404(User, pk=request.session['user_pk'])
        dr_addressee= get_object_or_404(User, pk=request.POST.get('reciever'))

        if dr_drafter and dr_addressee:
            report = DailyReport.objects.create(
                        dr_drafter = dr_drafter,
                        dr_addressee = dr_addressee,
                        dr_status = '1',
                        dr_title = "{0} 일일보고_{1}".format(date.today(), dr_drafter.user_name)
                    )

            objects_mappings = {
                'dc' : DailyContent,
                'tw' : TommrowWill,
                's' : Suggestion
            }

            contents = [
                ('dc', request.POST.getlist('dc_content')),
                ('tw', request.POST.getlist('tw_content')),
                ('s', request.POST.getlist('s_content'))
            ]

            for c_list in contents:
                if c_list[1]:
                    for v in c_list[1]:
                        quries = createQueries(c_list[0], v, report)
                        objects_mappings[c_list[0]].objects.create(**quries)
            
        return redirect('document:document-main')

def createQueries(prefix, value, obj):
    q = {}
    q['{0}_{1}'.format(prefix, 'report')] = obj
    q['{0}_{1}'.format(prefix, 'content')] = value
    return q

def ajax_userlist(request): 
        context = {
            'users' : User.objects.exclude(pk=request.session['user_pk']).order_by('user_rank')
        }
        return render(request, 'document/list/userlist.html', context)

def getPaginatorDatas(page=1, queryset=None, count=10):
    if queryset is None:
        return 0

    try:
        _page = Paginator(queryset, count)
        _list = _page.page(page).object_list
        _paginator_obj = _page.page(page)
        index = _paginator_obj.number - 1
        max_index = len(_page.page_range)
        start_index = index - 3 if index >= 3 else 0
        end_index = index + 3 if index <= max_index -3 else max_index
        _range = list(_page.page_range)[start_index:end_index]
        return _list, _paginator_obj, _range
    except Exception as e:
        return getPaginatorDatas(1, queryset)
